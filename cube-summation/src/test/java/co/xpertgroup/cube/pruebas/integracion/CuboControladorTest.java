package co.xpertgroup.cube.pruebas.integracion;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.xpertgroup.cube.CubeSummationApplication;
import co.xpertgroup.cube.dominio.CreacionCubo;
import co.xpertgroup.cube.dominio.Cubo;
import co.xpertgroup.cube.dominio.SumaSubCubo;

import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = CubeSummationApplication.class
)
public class CuboControladorTest {
	
	@Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;
    
    private static final ObjectMapper om = new ObjectMapper();
    
    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }
    
    /**
     * Test de integracion que prueba la generacion del cubo
     * @throws Exception
     */
    @Test
    @Rollback(true)
	public void cuboGenerar() throws Exception {
    
    	CreacionCubo creacionCubo = new CreacionCubo(2L, 0L);
    	mockMvc.perform(post("/cubo/generar" )
    	.content(om.writeValueAsString(creacionCubo))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", is(8)));

    }
    
    /**
     * Test de integracion que prueba la generacion del cubo
     * @throws Exception
     */
    @Test
    @Rollback(true)
	public void actualizarCubo() throws Exception {
    
    	cuboGenerar();
    	
    	Cubo cubo = new Cubo(1L, 1L ,1L,1L);
    	mockMvc.perform(post("/cubo/actualizar" )
    	.content(om.writeValueAsString(cubo))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.x", is(1)))
    	.andExpect(jsonPath("$.y", is(1)))
    	.andExpect(jsonPath("$.z", is(1)));
    }
    
    /**
     * Test de integracion que prueba la funcionalidad completa de obtenerSuma
     * @throws Exception
     */
    @Test
    @Rollback(true)
	public void obtenerSuma() throws Exception {
    
    	cuboGenerar();
    	
    	SumaSubCubo param = new SumaSubCubo(1L, 1L, 1L, 1L, 1L, 1L);
    	mockMvc.perform(post("/cubo/obtenerSuma" )
    	.content(om.writeValueAsString(param))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", is(0)));
  

    }

}
