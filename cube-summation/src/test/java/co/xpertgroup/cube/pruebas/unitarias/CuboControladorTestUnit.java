package co.xpertgroup.cube.pruebas.unitarias;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import co.xpertgroup.cube.controlador.CuboControlador;
import co.xpertgroup.cube.dominio.CreacionCubo;
import co.xpertgroup.cube.dominio.Cubo;
import co.xpertgroup.cube.dominio.SumaSubCubo;
import co.xpertgroup.cube.servicios.CuboServicio; 

public class CuboControladorTestUnit {

	CuboServicio cuboServicio;
	CuboControlador cuboControlador;
	
	@Before
    public void setUp() {
		cuboServicio = Mockito.mock(CuboServicio.class);
		cuboControlador = new CuboControlador(cuboServicio);
    }
	
	/**
	 * Test encargado de verificar el metodo 
	 * generarCubo del controlador
	 *
	 */
	@Test
    public void generarCuboTest() {
		 Mockito.when(cuboServicio.generarCubo(Mockito.anyLong(), Mockito.anyLong())).thenReturn(64L);
		 CreacionCubo creacionCubo = new CreacionCubo(4L, 0L);
		 Long result= cuboControlador.generarCubo(creacionCubo);
		 Assert.assertEquals(result.compareTo(64L),0L);
	}
	
	/**
	 * Test encargado de verificar el metodo 
	 * obtenerSumaSubCubo del controlador
	 *
	 */
	@Test
    public void obtenerSuma() {
		 Mockito.when(cuboServicio.obtenerSumaSubCubo(Mockito.anyLong(), Mockito.anyLong(), 
				 Mockito.anyLong(), Mockito.anyLong(), Mockito.anyLong(), Mockito.anyLong())).thenReturn(4L);
		 SumaSubCubo paramSuma = new SumaSubCubo(1L, 1L, 1L, 2L, 2L, 2L);
		 Long result= cuboControlador.obtenerSuma(paramSuma);
		 Assert.assertEquals(result.compareTo(4L),0L);
	}
	
	/**
	 * Test encargado de verificar el metodo 
	 * actualizarValor del controlador
	 *
	 */
	@Test
    public void actualizarValor() {
		 Mockito.when(cuboServicio.actualizarValor(Mockito.anyLong(), Mockito.anyLong(), 
				 Mockito.anyLong(), Mockito.anyLong())).thenReturn(new Cubo(1L, 1L, 1L, 1L));
		 Cubo cubo = new Cubo(1L, 1L, 1L, 1L);
		 Cubo cuboResult= cuboControlador.actualizarCubo(cubo);
		 Assert.assertEquals(cubo.getX().longValue(), cuboResult.getX().longValue());
		 Assert.assertEquals(cubo.getY().longValue(), cuboResult.getY().longValue());
		 Assert.assertEquals(cubo.getZ().longValue(), cuboResult.getZ().longValue());
		 Assert.assertEquals(cubo.getValor().longValue(), cuboResult.getValor().longValue());
	}
}
