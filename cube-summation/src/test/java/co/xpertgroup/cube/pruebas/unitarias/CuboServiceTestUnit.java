package co.xpertgroup.cube.pruebas.unitarias;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import co.xpertgroup.cube.dominio.Cubo;
import co.xpertgroup.cube.entidades.CuboEntidad;
import co.xpertgroup.cube.repositorio.CuboRepositorio;
import co.xpertgroup.cube.servicios.CuboServicio;
import co.xpertgroup.cube.servicios.implementacion.CuboServicioImpl;

public class CuboServiceTestUnit {

	CuboServicio cuboServicio;
	CuboRepositorio cuboRepositorio;

	@Before
	public void setUp() {
		cuboRepositorio = Mockito.mock(CuboRepositorio.class);
		cuboServicio = new CuboServicioImpl(cuboRepositorio);
	}

	/**
	 * Test encargado de verificar el metodo actualizarValor del servicio
	 *
	 */
	@Test
	public void actualizarValorTest() {
		CuboEntidad cuboEntidad = new CuboEntidad();
		cuboEntidad.setId(1L);
		cuboEntidad.setX(1L);
		cuboEntidad.setY(1L);
		cuboEntidad.setZ(1L);
		Optional<CuboEntidad> cuboOpcional = Optional.of(cuboEntidad);
		Mockito.when(cuboRepositorio.consultarCubo(Mockito.anyLong(), Mockito.anyLong(), Mockito.anyLong()))
				.thenReturn(cuboOpcional);

		Cubo cuboResult = cuboServicio.actualizarValor(1L, 1L, 1L, 1L);
		Assert.assertEquals(cuboResult.getX().longValue(), cuboEntidad.getX().longValue());
		Assert.assertEquals(cuboResult.getY().longValue(), cuboEntidad.getY().longValue());
		Assert.assertEquals(cuboResult.getZ().longValue(), cuboEntidad.getZ().longValue());
	}

	/**
	 * Test encargado de verificar el metodo obtenerSumaSubCubo del servicio
	 *
	 */
	@Test
	public void obtenerSumaSubCuboTest() {

		Mockito.when(cuboRepositorio.obtenerSumaSubCubo(Mockito.anyLong(), Mockito.anyLong(), Mockito.anyLong(),
				Mockito.anyLong(), Mockito.anyLong(), Mockito.anyLong())).thenReturn(0L);

		Mockito.when(cuboRepositorio.obtenerMaximaPos()).thenReturn(1L);

		Long result = cuboServicio.obtenerSumaSubCubo(1L, 1L, 1L, 1L, 1L, 1L);
		Assert.assertEquals(result.longValue(), 0);
	}

	
}
