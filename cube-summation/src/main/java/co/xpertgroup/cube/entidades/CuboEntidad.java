package co.xpertgroup.cube.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.UtilityClass;

@Entity
@Table(name = CuboEntidad.TableInfo.TABLE_NAME)
@Setter
@Getter
public class CuboEntidad {


		@UtilityClass
		public static final class TableInfo {
			public static final String TABLE_NAME = "cubo";
			public static final String ID = "id";
			public static final String X = "x";
			public static final String Y = "y";
			public static final String Z = "z";
			public static final String VALOR = "valor";
		}

		@UtilityClass
		public static final class ClassInfo {
			public static final String ID = "id";
			public static final String X = "x";
			public static final String Y = "y";
			public static final String Z = "z";
			public static final String VALOR = "valor";
			
		}

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = TableInfo.ID)
		@NonNull
		private Long id;
		
		@Column(name = TableInfo.X)
		@NonNull
		private Long x;
		
		@Column(name = TableInfo.Y)
		@NonNull
		private Long y;
		
		@Column(name = TableInfo.Z)
		@NonNull
		private Long z;
		
		@Column(name = TableInfo.VALOR)
		@NonNull
		private Long valor;

	}
