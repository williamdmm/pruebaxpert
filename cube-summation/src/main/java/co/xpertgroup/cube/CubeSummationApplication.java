package co.xpertgroup.cube;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CubeSummationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CubeSummationApplication.class, args);
	}

}
