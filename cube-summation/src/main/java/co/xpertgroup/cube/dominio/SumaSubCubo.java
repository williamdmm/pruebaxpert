package co.xpertgroup.cube.dominio;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.UtilityClass;

@Getter
@Setter
public class SumaSubCubo {

	
	@NonNull
	private Long x1;

	@NonNull
	private Long y1;

	@NonNull
	private Long z1;

	@NonNull
	private Long x2;

	@NonNull
	private Long y2;

	@NonNull
	private Long z2;

	private SumaSubCubo() {
		// Usado para la deseralizaciÃ³n de Jackson
	}

	public SumaSubCubo(Long x1, Long y1, Long z1, Long x2, Long y2, Long z2) {
		super();
		this.x1 = x1;
		this.y1 = y1;
		this.z1 = z1;
		this.x2 = x2;
		this.y2 = y2;
		this.z2 = z2;
	}

	



}
