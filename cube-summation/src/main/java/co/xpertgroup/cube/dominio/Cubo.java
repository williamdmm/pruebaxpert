package co.xpertgroup.cube.dominio;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.UtilityClass;

@Getter
@Setter
public class Cubo {

	@UtilityClass
	public static final class ClassInfo {
		public static final String ID = "id";
		public static final String X = "x";
		public static final String Y = "y";
		public static final String Z = "z";
		public static final String VALOR = "valor";

	}

	
	private Long id;

	@NonNull
	private Long x;

	@NonNull
	private Long y;

	@NonNull
	private Long z;

	@NonNull
	private Long valor;

	private Cubo() {
		// Usado para la deseralizaciÃ³n de Jackson
	}

	public Cubo( @NonNull Long x, @NonNull Long y, @NonNull Long z, @NonNull Long valor) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
		this.valor = valor;
	}
	
	public Cubo( @NonNull Long x, @NonNull Long y, @NonNull Long z) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}

}
