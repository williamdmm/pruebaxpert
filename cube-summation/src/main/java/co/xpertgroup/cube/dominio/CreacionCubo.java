package co.xpertgroup.cube.dominio;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.UtilityClass;

@Getter
@Setter
public class CreacionCubo {

	@UtilityClass
	public static final class ClassInfo {
		public static final String TAMANIO = "tamanio";
		public static final String VALOR = "valor";

	}

	@NonNull
	private Long tamanio;

	@NonNull
	private Long valor;

	private CreacionCubo() {
		// Usado para la deseralizaciÃ³n de Jackson
	}

	public CreacionCubo(@NonNull Long tamanio, @NonNull Long valor) {
		super();
		this.tamanio = tamanio;
		this.valor = valor;
	}

}
