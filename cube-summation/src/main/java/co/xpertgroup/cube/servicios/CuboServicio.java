package co.xpertgroup.cube.servicios;

import co.xpertgroup.cube.dominio.Cubo;

public interface CuboServicio {

	/**
	 * firma del metodo que obtiene la suma del sub cubo 
	 * formado por los parametros dados
	 * @param x1 posicion en x inicial
	 * @param x2 posicion en x final
	 * @param y1 posicion en y inicial
	 * @param y2 posicion en y final
	 * @param z1 posicion en z inicial
	 * @param z2 posicion en z final
	 * @return
	 */
	public Long obtenerSumaSubCubo( Long x1, Long y1, Long z1, Long x2, Long y2, Long z2);
	
	/**
	 * firma del metodo encargado de generar el cubo dado en tamaño
	 */
	public Long generarCubo(Long tamanio, Long valorDefacto);

	/**
	 * firma del metodo encargado de actualizar el valor dado la posicion
	 * @param x posicion en x
	 * @param y posicion en y
	 * @param z posicion en z
	 * @param valor nuevo valor
	 * @return
	 */
	public Cubo actualizarValor(Long x, Long y, Long z, Long valor);
	
	
}
