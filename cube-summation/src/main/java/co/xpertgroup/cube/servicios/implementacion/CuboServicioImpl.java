package co.xpertgroup.cube.servicios.implementacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.xpertgroup.cube.dominio.Cubo;
import co.xpertgroup.cube.entidades.CuboEntidad;
import co.xpertgroup.cube.excepciones.NoFoundDataException;
import co.xpertgroup.cube.repositorio.CuboRepositorio;
import co.xpertgroup.cube.servicios.CuboServicio;

@Service
public class CuboServicioImpl implements CuboServicio {

	
	protected CuboRepositorio cuboRepositorio;
	
	@Autowired
	public CuboServicioImpl(CuboRepositorio cuboRepositorio) {
		super();
		this.cuboRepositorio = cuboRepositorio;
	}

	/**
	 * Metodo encargado de generar el cubo dado un tamaño 
	 */
	@Transactional
	public Long generarCubo(Long tamanio, Long valorDefecto) {
		borrarCubo();
		return cuboRepositorio.crearCuboInicial(tamanio, valorDefecto);
	}

	/**
	 * Metodo encargado de obtener la suma del cubo dado 
	 * las posiciones de un sub cubo
	 *
	 */
	@Transactional(readOnly=true)
	public Long obtenerSumaSubCubo(Long x1, Long y1, Long z1, Long x2, Long y2, Long z2) {
		
		if(validarPosicionCubo(new Cubo(x1, y1, z1)) && validarPosicionCubo(new Cubo(x2, y2, z2)) ) {
			return cuboRepositorio.obtenerSumaSubCubo(x1, y1, z1, x2, y2, z2);
		}else {
			 throw new NoFoundDataException();
		}
		
		
	}


	/**
	 * Metodo encargado de actualizar el valor dado la posicion
	 */
	@Transactional
	public Cubo actualizarValor(Long x, Long y, Long z, Long valor) {
		CuboEntidad cuboEntity = buscarCubo(x, y, z);
		cuboEntity.setValor(valor);
		cuboRepositorio.save(cuboEntity);
		return new Cubo(x, y, z, valor);
	}
	
	/**
	 * MEtodo encargado de buscar un cuboEntidad dado un x, y , z
	 * @param x
	 * @param y
	 * @param z
	 * @return cuboentidad
	 */
	public CuboEntidad buscarCubo(Long x, Long y, Long z) {
		return cuboRepositorio.consultarCubo(x, y, z).orElseThrow(NoFoundDataException::new);
	}

	/**
	 * Metodo encargado de borrar el cubo
	 */
	public void borrarCubo() {
		cuboRepositorio.deleteAll();
	}
	
	/**
	 * MEtodo encargado de validar la posicion del cubo
	 * @param cubo
	 * @return true si es una posicion valid
	 */
	public Boolean validarPosicionCubo(Cubo cubo) {
		Long posMaxima = cuboRepositorio.obtenerMaximaPos();
		return ( cubo.getX() <= posMaxima && cubo.getY() <= posMaxima && cubo.getZ() <= posMaxima);
	}
	
	
}
