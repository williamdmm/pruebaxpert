package co.xpertgroup.cube.repositorio;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.xpertgroup.cube.entidades.CuboEntidad;
import co.xpertgroup.cube.repositorio.custom.CuboRepositorioCustom;

@Repository
public interface CuboRepositorio extends JpaRepository<CuboEntidad, Long>, CuboRepositorioCustom{
	

	@Query("select sum(c.valor) from CuboEntidad c where c.x >= :x1 and c.x <= :x2 and c.y >= :y1 and c.y <= :y2 and c.z >= :z1 and c.z <= :z2")
	public Long obtenerSumaSubCubo(@Param("x1") Long x1,@Param("y1") Long y1,@Param("z1") Long z1,@Param("x2") Long x2,@Param("y2") Long y2,@Param("z2") Long z2);

	@Query("select c from CuboEntidad c where c.x = :x1 and c.y = :y1 and c.z = :z1")
	public Optional<CuboEntidad> consultarCubo(@Param("x1") Long x,@Param("y1") Long y, @Param("z1") Long z);

	@Query("select max(c.x) from CuboEntidad c ")
	public Long obtenerMaximaPos();

	
}
