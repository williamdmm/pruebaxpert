package co.xpertgroup.cube.repositorio.custom;

public interface CuboRepositorioCustom {

	/**
	 * firma del metodo encargado de crear un 
	 * cubo dado el tamaño
	 * @param tamanio
	 * @return 
	 */
	public Long crearCuboInicial(Long tamanio, Long valorDefecto);
	
	
}
