package co.xpertgroup.cube.repositorio.implementacion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import org.springframework.stereotype.Repository;

import co.xpertgroup.cube.entidades.CuboEntidad;
import co.xpertgroup.cube.repositorio.custom.CuboRepositorioCustom;

@Repository
public class CuboRepositorioImpl implements CuboRepositorioCustom {

	@PersistenceContext
	protected EntityManager entityManager;

	/**
	 * metodo encargado de crear un 
	 * cubo dado el tamaño
	 */
	public Long crearCuboInicial(Long tamanio, Long valorDefecto) {
		int numeroPersist = 0;
		for (int i = 1; i <= tamanio; i++) {

			for (int j = 1; j <= tamanio; j++) {

				for (int k = 1; k <= tamanio; k++) {

					CuboEntidad cuboNew = new CuboEntidad();
					cuboNew.setX(Long.valueOf(i));
					cuboNew.setY(Long.valueOf(j));
					cuboNew.setZ(Long.valueOf(k));
					cuboNew.setValor(valorDefecto);

					entityManager.persist(cuboNew);
					numeroPersist++;
				}
			}
		}
		entityManager.flush();
		return Long.valueOf(numeroPersist);
	}

	


	
}
