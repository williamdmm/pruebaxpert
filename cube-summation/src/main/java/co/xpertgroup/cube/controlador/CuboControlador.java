package co.xpertgroup.cube.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.xpertgroup.cube.dominio.CreacionCubo;
import co.xpertgroup.cube.dominio.Cubo;
import co.xpertgroup.cube.dominio.SumaSubCubo;
import co.xpertgroup.cube.servicios.CuboServicio;

@RestController
@RequestMapping(CuboControlador.Paths.CONTROLLER)
@CrossOrigin(origins = "*")
public class CuboControlador {

	public static class Paths {
		private Paths() {}
		public static final String CONTROLLER = "/cubo";
		public static final String GENERAR = "/generar";
		public static final String OBTENER_SUMA = "/obtenerSuma";
		public static final String ACTUALIZAR = "/actualizar";
	}
	
	
	private CuboServicio cuboServicio;

	@Autowired
    public CuboControlador(CuboServicio cuboServicio) {
        this.cuboServicio = cuboServicio;
    }
	
	/**
	 * Metodo encargado de recibir la peticion y llamar al servicio 
	 * encargado de genrar el cubo segun el tamaño
	 * @return retorna el numero total de filas creadas
	 */
	@PostMapping(value= Paths.GENERAR, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Long generarCubo(@RequestBody(required = true) CreacionCubo creacionCubo ) {
		return cuboServicio.generarCubo(creacionCubo.getTamanio(), creacionCubo.getValor());
	}
	
	/**
	 * Metodo encargado de recibir la peticion y llamar al servicio 
	 * encargado de actualizar el cubo segun los parametros
	 * @return retorna el cubo 
	 */
	@PostMapping(value= Paths.ACTUALIZAR, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Cubo actualizarCubo(@RequestBody(required = true) Cubo cubo ) {
		return cuboServicio.actualizarValor(cubo.getX(), cubo.getY(), cubo.getY(), cubo.getValor());
	}
	
	
	/**
	 * Metodo encargado de recibir la peticion y llamar al servicio 
	 * encargado de realizar la suma del sub cubo segun los parametros
	 * @return retorna el valor de la suma 
	 */
	@PostMapping(value= Paths.OBTENER_SUMA, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Long obtenerSuma(@RequestBody(required = true) SumaSubCubo paramSuma ) {
		return cuboServicio.obtenerSumaSubCubo(paramSuma.getX1(), paramSuma.getY1(), paramSuma.getZ1(), paramSuma.getX2(), paramSuma.getY2(), paramSuma.getZ2());
	}
	
	

}
