import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule}    from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
//componentes
import { AppComponent } from './app.component';
import { CreacionCuboComponent } from './componentes/creacion-cubo/creacion-cubo.component';
import { ActualizarCuboComponent } from './componentes/actualizar-cubo/actualizar-cubo.component';
import { SumaCuboComponent } from './componentes/suma-cubo/suma-cubo.component';

//servicios
import { CuboService } from './servicios/cubo.service';

//modulos
import { AppRoutingModule } from './app-routing.module';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {CardModule} from 'primeng/card';

@NgModule({
  declarations: [
    AppComponent,
    CreacionCuboComponent,
    ActualizarCuboComponent,
    SumaCuboComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    InputTextModule,
    ButtonModule,
    CardModule,


  ],
  providers: [
    CuboService,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
