import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CuboDTO } from '../dtos/cubo';
import { RequestConstants } from '../utiles/request.constants';
import { CreacionCuboDTO } from '../dtos/creacionCubo';
import { SumaSubCuboDTO } from '../dtos/sumaSubCubo';


@Injectable()
export class CuboService {
    constructor(private http: HttpClient) { }

    /**
     * Metodo encargado de realizar la peticion al servicio de generacion de cubo
     * enviandole como parametro el dto creacionCuboDTO
     * @param cubo 
     */
    public crearCubo(cubo:CreacionCuboDTO): Observable<any> {
        return this.http.post(RequestConstants.URL_CUBO_GENERAR,cubo);
    }

    /**
     * Metodo encargado de realizar la peticion al servicio de actualizacion de valor
     * en viandole como parametro el dto CuboDTO , el cual contiene la psoicion 
     * en x,y,z y el valor 
     * @param cubo 
     */
    public updateCubo(cubo:CuboDTO): Observable<any> {
        return this.http.post(RequestConstants.URL_CUBO_ACTUALIZAR,cubo);
    }

    /**
     * Metodo encargado de realizar la peticion al servicio de actualizacion de valor
     * en viandole como parametro el dto CuboDTO , el cual contiene la psoicion 
     * en x,y,z y el valor 
     * @param cubo 
     */
    public obtenerSuma(paramSuma:SumaSubCuboDTO): Observable<any> {
        return this.http.post(RequestConstants.URL_CUBO_OBTENER_SUMA,paramSuma);
    }


}