export class PathConstants {


    public static PATH_EMPTY: string = "";
    public static PATH_CUBO: string = "cubo";
    public static PATH_CUBO_CREACION: string = PathConstants.PATH_CUBO+"/"+"creacion";
    public static PATH_CUBO_ACTUALIZACION: string = PathConstants.PATH_CUBO+"/"+"actualizar";
    public static PATH_CUBO_SUMA: string = PathConstants.PATH_CUBO+"/"+"suma";
    
}
