export class RequestConstants {

    public static URL_BASE: string = "http://localhost:8080";
    public static URL_CUBO: string = "cubo";
    public static URL_CUBO_GENERAR: string = RequestConstants.URL_BASE+"/"+ RequestConstants.URL_CUBO+"/"+"generar";
    public static URL_CUBO_ACTUALIZAR: string = RequestConstants.URL_BASE+"/"+ RequestConstants.URL_CUBO+"/"+"actualizar";
    public static URL_CUBO_OBTENER_SUMA: string = RequestConstants.URL_BASE+"/"+ RequestConstants.URL_CUBO+"/"+"obtenerSuma";

}
