export const messages = {

    "error.tamanio.min.cubo": "el tamaño del cubo debe ser mayor a 0",
    "error.tamanio.max.cubo": "el tamaño del cubo debe ser menor o igual a 100",
    "error.valor.min.cubo": "el valor por defecto debe ser mayor o igual a cero",

    "error.falta.x": "valor invalido para la posicion en x",
    "error.falta.y": "valor invalido para la posicion en y",
    "error.falta.z": "valor invalido para la posicion en z",
    "error.falta.valor": "valor invalido para el campo valor ",
    "error.en.server": "No existe la posicion del cubo",

    "error.falta.x1": "valor invalido para la posicion en x1",
    "error.falta.y1": "valor invalido para la posicion en y1",
    "error.falta.z1": "valor invalido para la posicion en z1",

    "error.falta.x2": "valor invalido para la posicion en x2",
    "error.falta.y2": "valor invalido para la posicion en y2",
    "error.falta.z2": "valor invalido para la posicion en z2",

    "error.en.server.posicion": "No existe alguna posicion del cubo",

    "sucess.crear.cubo": "Se creo el cubo con exito",
    "sucess.actualizo.cubo": "Se actualizo el cubo con exito",
    "sucess.suma": "El resultado de la suma es: ",
};
