import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreacionCuboComponent } from './componentes/creacion-cubo/creacion-cubo.component';
import { ActualizarCuboComponent } from './componentes/actualizar-cubo/actualizar-cubo.component';
import { SumaCuboComponent } from './componentes/suma-cubo/suma-cubo.component';

import { PathConstants } from './utiles/path.constants';


const routes: Routes = [
    { path: PathConstants.PATH_CUBO_CREACION, component: CreacionCuboComponent },
    { path: PathConstants.PATH_CUBO_ACTUALIZACION, component: ActualizarCuboComponent },
    { path: PathConstants.PATH_CUBO_SUMA, component: SumaCuboComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }