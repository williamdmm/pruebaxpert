import { Component, OnInit } from '@angular/core';
import { CuboService } from '../../servicios/cubo.service';
import { CuboDTO } from '../../dtos/cubo';
import { messages } from '../../utiles/message.constants';
import { SumaSubCuboDTO } from '../../dtos/sumaSubCubo';
@Component({
  selector: 'app-suma-cubo',
  templateUrl: './suma-cubo.component.html',

})
export class SumaCuboComponent implements OnInit {

  /**
   * atributo que contiene los valores necesarios para actualizar el cubo
   */
  public sumaSubCubo: SumaSubCuboDTO = new SumaSubCuboDTO();

  public errores: Array<string> = [];

  public mensajes: Array<string> = [];

  constructor(protected _cuboServicio: CuboService) { }

  ngOnInit() {

  }

  /** 
   * Metodo encargado de llamar al servicio para actualizar el cubo
  */
  public actualizarCubo(paramSuma: SumaSubCuboDTO) {
    if (this.validarSumaSubCubo) {
      this._cuboServicio.obtenerSuma(paramSuma).subscribe(result => {
        this.escribirMensaje(messages["sucess.suma"] + result);
      }, error => {
        this.errores = [];
        this.errores.push(messages["error.en.server.posicion"]);
      });
    }
  }

  /** 
   * Metodo encargado de verificar que los datos del cubo esten completos
   *  para ser actualizados
  */
  private validarSumaSubCubo(): void {
    this.errores = [];
    if (!this.sumaSubCubo) {
    }
    if (!this.sumaSubCubo.x1 || this.sumaSubCubo.x1 <= 0) {
      this.errores.push(messages["error.falta.x1"]);
    }
    if (!this.sumaSubCubo.y1 || this.sumaSubCubo.y1 <= 0) {
      this.errores.push(messages["error.falta.y1"]);
    }
    if (!this.sumaSubCubo.z1 || this.sumaSubCubo.z1 <= 0) {
      this.errores.push(messages["error.falta.z1"]);
    }
    if (!this.sumaSubCubo.x2 || this.sumaSubCubo.x2 <= 0) {
      this.errores.push(messages["error.falta.x2"]);
    }
    if (!this.sumaSubCubo.y2 || this.sumaSubCubo.y2 <= 0) {
      this.errores.push(messages["error.falta.y2"]);
    }
    if (!this.sumaSubCubo.z2 || this.sumaSubCubo.z2 <= 0) {
      this.errores.push(messages["error.falta.z2"]);
    }
    if (this.errores.length == 0) {
      this.actualizarCubo(this.sumaSubCubo);
    }

  }

  /**
   * Metodo encargado de mostrar y quitar un mensaje de exito
   * @param mensaje 
   */
  public escribirMensaje(mensaje: string) {
    this.mensajes = [];
    this.mensajes.push(mensaje);
    setTimeout(() => {
      this.mensajes = [];
    }, 3000);
  }
}
