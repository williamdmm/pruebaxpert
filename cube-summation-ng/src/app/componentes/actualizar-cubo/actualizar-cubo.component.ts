import { Component, OnInit } from '@angular/core';
import { CuboService } from '../../servicios/cubo.service';
import { CuboDTO } from '../../dtos/cubo';
import { messages } from '../../utiles/message.constants';
@Component({
  selector: 'app-actualizar-cubo',
  templateUrl: './actualizar-cubo.component.html',
})
export class ActualizarCuboComponent implements OnInit {


  /**
   * atributo que contiene los valores necesarios para actualizar el cubo
   */
  public cubo: CuboDTO = null;

  public errores: Array<string> = [];

  public mensajes:Array<string>=[];

  constructor(protected _cuboServicio: CuboService) { }

  ngOnInit() {
    /**
     * se inicializa el valor en 0
     */
    this.cubo = new CuboDTO();
    this.cubo.x = 0;
    this.cubo.y = 0;
    this.cubo.z = 0;
    this.cubo.valor = 0;
  }

  /** 
   * Metodo encargado de llamar al servicio para actualizar el cubo
  */
  public actualizarCubo(cubo: CuboDTO) {
    if (this.validarActualizacionCubo) {
      this._cuboServicio.updateCubo(cubo).subscribe(result => {
        this.escribirMensaje(messages["sucess.actualizo.cubo"]);
      },
        error => {
          console.log(error);
          this.errores = [];
          this.errores.push(messages["error.en.server"]);
        });
    }
  }

  /** 
   * Metodo encargado de verificar que los datos del cubo esten completos
   *  para ser actualizados
  */
  private validarActualizacionCubo():void {
    this.errores = [];
    if (!this.cubo.x || this.cubo.x <= 0) {
      this.errores.push(messages["error.falta.x"]);
    }
    if (!this.cubo.y || this.cubo.y <= 0) {
      this.errores.push(messages["error.falta.y"]);
    }
    if (!this.cubo.z || this.cubo.z <= 0) {
      this.errores.push(messages["error.falta.z"]);
    }
    if (!this.cubo.valor || this.cubo.x < 0) {
      this.errores.push(messages["error.falta.valor"]);
    }

    if(this.errores.length==0){
      this.actualizarCubo(this.cubo);
    }

  }

   /**
   * Metodo encargado de mostrar y quitar un mensaje de exito
   * @param mensaje 
   */
  public escribirMensaje(mensaje:string){
    this.mensajes=[];
    this.mensajes.push(mensaje);
    setTimeout(()=>{
      this.mensajes=[];
    }, 3000);
  }
}
