import { Component, OnInit } from '@angular/core';
import { CuboService } from '../../servicios/cubo.service';
import { CreacionCuboDTO } from '../../dtos/creacionCubo';
import { messages } from '../../utiles/message.constants'; 
@Component({
  selector: 'app-creacion-cubo',
  templateUrl: './creacion-cubo.component.html',
})
export class CreacionCuboComponent implements OnInit {

  /**atributo que contiene los datos para la creacion del cubo */
  public creacionCubo: CreacionCuboDTO=new CreacionCuboDTO();

  public errores:Array<string>=[];

  public mensajes:Array<string>=[];

  public loading = false;

  constructor(protected _cuboServicio: CuboService) { }

  ngOnInit() {
    /** Se configura que el valor por defecto sea 0 */
    this.creacionCubo.valor=0;
    /** se configura que el valor por defecto sea 1 */
    this.creacionCubo.tamanio=0;
    }

  /** 
   * Metodo encargado de llamar al servicio encargado de crear el cubo
  */
  private crearCubo():void{
    this.loading=true;
      this._cuboServicio.crearCubo(this.creacionCubo).subscribe(result=>{
        this.escribirMensaje(messages["sucess.crear.cubo"]);
        this.loading=false;
      });
  }

  /** Metoo encargado de validar los campos necesario para 
   * la creacion del cubo 
  */
  public validarCreacionCubo(){
    this.errores=[];
    if(this.creacionCubo.tamanio<=0){
      this.errores.push(messages["error.tamanio.min.cubo"]);
    }
    if(this.creacionCubo.tamanio>100){
      this.errores.push(messages["error.tamanio.max.cubo"]);
    }
    if(this.creacionCubo.valor<0){
      this.errores.push(messages["error.valor.min.cubo"]);
    }
    if(this.errores.length==0){
      this.crearCubo();
    }
    
  }


  /**
   * Metodo encargado de mostrar y quitar un mensaje de exito
   * @param mensaje 
   */
  public escribirMensaje(mensaje:string){
    this.mensajes=[];
    this.mensajes.push(mensaje);
    setTimeout(()=>{
      this.mensajes=[];
    }, 3000);
  }
}
